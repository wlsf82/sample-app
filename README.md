# Sample App Cypress tests

This project intends to demonstrate the third guideline of [Better Code Hub](http://bettercodehub.com) (Write Code Once) with GUI tests written using [Cypress](http://cypress.io) for a sample web app.

## Pre-requirements

You will need [Node](http://nodejs.org) and NPM to run the tests for this sample app.

> I used the following verion of node when creating this project v10.15.1, and the following version of npm 6.7.0.

> npm is automatically installed once node is installed.

## Installation

Run `npm i` to install the dev dependencies.

## Running tests

Run `npx cypress open` to open cypress, then click the **Run all specs** button to run the tests in interactive mode.

Or run `npm t` to run the tests in headless mode.

### Build status

[![Build Status](https://gitlab.com/wlsf82/sample-app/badges/master/build.svg)](https://gitlab.com/wlsf82/sample-app)

___

Made with 💚 by [Walmyr Lima e Silva Filho](https://walmyr-filho.com).