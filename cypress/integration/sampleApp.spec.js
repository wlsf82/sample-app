/// <reference types="cypress" />

// v1 - Código duplicado
describe("Sample app", () => {
  context("Shorten/Expand", () => {
    it("shortens a URL", () => {
      cy.visit("https://goo.gl/ZYQsTL");

      cy.get("input[type='text']").type(
        "https://abcdefghijklmnopqrstuvwxyz.æåø"
      );
      cy.contains("Shorten").click();

      cy.get("#responseField a").should("contain", "https://foo.bar");
    });

    it("expands a URL", () => {
      cy.visit("https://goo.gl/ZYQsTL");

      cy.get("input[type='text']").type("https://abc.de");
      cy.contains("Expand").click();

      cy.get("#responseField a").should(
        "contain",
        "https://foo.bar.baz.bah.boo"
      );
    });
  });
});

// v2 - Eliminação de duplicação de código com o uso de funções
describe("Sample app", () => {
  context("Shorten/Expand", () => {
    beforeEach(() => cy.visit("https://goo.gl/ZYQsTL"));

    it("shortens a URL", () => {
      shortenUrl("https://abcdefghijklmnopqrstuvwxyz.æåø");

      verifyResponseFieldLinkContainUrl("https://foo.bar");
    });

    it("expands a URL", () => {
      expandUrl("https://abc.de");

      verifyResponseFieldLinkContainUrl("https://foo.bar.baz.bah.boo");
    });

    function shortenUrl(url) {
      typeUrl(url);
      cy.contains("Shorten").click();
    }

    function expandUrl(url) {
      typeUrl(url);
      cy.contains("Expand").click();
    }

    function typeUrl(url) {
      cy.get("input[type='text']").type(url);
    }

    function verifyResponseFieldLinkContainUrl(url) {
      cy.get("#responseField a").should("contain", url);
    }
  });
});

// v3 - Limpesa de código com o uso de commandos customizados
describe("Sample app", () => {
  context("Shorten/Expand", () => {
    beforeEach(() => cy.visit("https://goo.gl/ZYQsTL"));

    it("shortens a URL", () => {
      cy.shortenUrl("https://abcdefghijklmnopqrstuvwxyz.æåø");

      cy.verifyResponseFieldLinkContainUrl("https://foo.bar");
    });

    it("expands a URL", () => {
      cy.expandUrl("https://abc.de");

      cy.verifyResponseFieldLinkContainUrl("https://foo.bar.baz.bah.boo");
    });
  });
});
