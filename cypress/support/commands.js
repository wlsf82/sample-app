Cypress.Commands.add("shortenUrl", url => {
  cy.typeUrl(url);
  cy.contains("Shorten").click();
});

Cypress.Commands.add("expandUrl", url => {
  cy.typeUrl(url);
  cy.contains("Expand").click();
});

Cypress.Commands.add("typeUrl", url => {
  cy.get("input[type='text']").type(url);
});

Cypress.Commands.add("verifyResponseFieldLinkContainUrl", url => {
  cy.get("#responseField a").should("contain", url);
});
